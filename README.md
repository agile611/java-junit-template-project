# Java + JUnit + Ant Template Project

----------
A Java template project pre configured with JUnit and Ant.

## Quick Start

Clone or download this repo

```shell
    git clone https://guillemhs@bitbucket.org/itnove/java-junit-template-project.git
```

## Installation for Local Development

Run `ant` to get the unit tests going

```shell
    ant test
```