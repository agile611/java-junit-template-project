package helloWorld;

import static org.junit.Assert.*;

import org.junit.Test;

import com.itnove.junit.helloWorld.HelloWorld;

public class HelloWorldTest {
    @Test
    public void testHellowWorld() {
        HelloWorld hello = new HelloWorld();
        System.out.println("A test for Hello World String, Hello World" + hello.sayHello());
        assertEquals("A test for Hello World String", "Hello World", hello.sayHello());
    }
}
